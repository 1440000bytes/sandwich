# Sandwich

## Electrum plugin to create basic OP_CTV transactions
- Clone repository and copy the plugin directory in electrum/electrum/plugins directory
- `./run_electrum --signet`
- Enable 'sandwich' plugin in settings and relaunch electrum
- Enter a new address and amount that could be the only output for a CTV UTXO
- Select an input to the fund the transaction
- 2 transaction hex would be printed: a) Lock bitcoin in CTV UTXO b) Unlock bitcoin from CTV UTXO
- Use [bitcoin-inquisition](https://github.com/bitcoin-inquisition/bitcoin) to broadcast these transactions

![output](https://gitlab.com/1440000bytes/sandwich/-/raw/main/images/output.png)
![input](https://gitlab.com/1440000bytes/sandwich/-/raw/main/images/input.png)
![txs](https://gitlab.com/1440000bytes/sandwich/-/raw/main/images/txs.png)



