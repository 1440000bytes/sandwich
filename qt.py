

from PyQt5.QtWidgets import QTabWidget, QWidget, QVBoxLayout, QLineEdit, QPushButton, QLabel, QFormLayout, QListWidget, QListWidgetItem, QTextEdit
from electrum.i18n import _
from electrum.plugin import BasePlugin, hook
from PyQt5.QtCore import Qt, pyqtSignal, QObject
from bitcoin import SelectParams
from bitcoin.core import (
    CTransaction,
    CMutableTransaction,
    CMutableTxIn,
    CTxIn,
    CTxOut,
    CScript,
    COutPoint,
    CTxWitness,
    CTxInWitness,
    CScriptWitness,
    COIN,
    lx,
)
from bitcoin.core import script
from bitcoin.wallet import CBech32BitcoinAddress, P2WPKHBitcoinAddress, CBitcoinSecret
from buidl.hd import HDPrivateKey, PrivateKey
from buidl.ecc import S256Point
import struct
import hashlib
from dataclasses import dataclass
import hashlib
import asyncio

class SignalEmitter(QObject):
    show_inputlist_signal = pyqtSignal(QWidget, QWidget, object)

class Plugin(BasePlugin):

    def __init__(self, parent, config, name):
        BasePlugin.__init__(self, parent, config, name)
        self.signal_emitter = SignalEmitter()
        self.address_text_fields = {}
        SelectParams('signet')

        self.OP_CHECKTEMPLATEVERIFY = script.OP_NOP4
        self.TX_FEE = 1000

    @hook
    def load_wallet(self, wallet, window):
        if not self.is_enabled():
            return

        tab_widget = QTabWidget()
        tab_widget.setObjectName("sandwich_tab")

        widget = QWidget()
        layout = QVBoxLayout(widget)

        form_layout = QFormLayout()

        address_label = QLabel("Bitcoin address:")
        self.address_text_fields[wallet] = QLineEdit()
        self.address_text_fields[wallet].setFixedWidth(500)

        form_layout.addRow(address_label, self.address_text_fields[wallet])

        amount_label = QLabel("Amount:")
        self.amount_text_field = QLineEdit()
        self.amount_text_field.setFixedWidth(200)

        form_layout.addRow(amount_label, self.amount_text_field)

        submit_button = QPushButton("Next")
        submit_button.setFixedWidth(300)

        layout.addLayout(form_layout)
        layout.addWidget(submit_button, alignment=Qt.AlignCenter)

        tab_widget.addTab(widget, _("Output Restrictions"))

        window.tabs.addTab(tab_widget, _("Sandwich"))

        submit_button.clicked.connect(lambda: self.restrict_output(
            wallet,
            self.address_text_fields[wallet].text(),
            float(self.amount_text_field.text()),
            tab_widget
        ))

        self.signal_emitter = SignalEmitter()
        self.signal_emitter.show_inputlist_signal.connect(self.show_inputlist)


    def show_inputlist(self, tab_widget, input_selection_widget, wallet):
        layout = input_selection_widget.layout()

        select_label = QLabel("Select an input from the list:")
        select_label.setAlignment(Qt.AlignCenter)
        layout.addWidget(select_label)

        input_list = QListWidget()

        for utxo in wallet.get_utxos():
            item = QListWidgetItem(str(utxo.prevout.txid.hex() + ":" + str(utxo.prevout.out_idx)))
            item.setData(Qt.UserRole, utxo)
            input_list.addItem(item)
        layout.addWidget(input_list)

        prepare_button = QPushButton("Prepare sandwich")
        layout.addWidget(prepare_button)
        prepare_button.setFixedWidth(300)

        layout.setAlignment(prepare_button, Qt.AlignCenter)

        def on_prepare_button_clicked():
            
            selected_items = input_list.selectedItems()
            if selected_items:
                selected_item = selected_items[0]
                selected_input = selected_item.data(Qt.UserRole)
                
                public_key = bytes.fromhex(wallet.get_public_keys(selected_input.address)[0])
                utxo_address = P2WPKHBitcoinAddress.from_scriptPubKey(CScript([script.OP_0, script.Hash160(public_key)]))
                private_key=CBitcoinSecret(wallet.export_private_key(selected_input.address, None).split(":")[1])

                lock_tx = self.ctv_tx(
                    amount=int(self.out_amount * COIN),
                    vin_txid=selected_input.prevout.txid.hex(),
                    vin_index=selected_input.prevout.out_idx,
                    utxo_address=utxo_address,
                    funding_prvkey=private_key,
                    funding_pubkey=public_key
                )
                
                unlock_tx = self.spend_tx(
                    amount=int(self.out_amount * COIN) - (2*self.TX_FEE),
                    vin_txid=self.get_txid(lock_tx).hex(),
                    vin_index=0,
                )

                tx_tab_widget = QTabWidget()
                tx_tab_widget.setObjectName("tx_tab")

                tx_widget = QWidget()
                tx_layout = QVBoxLayout(tx_widget)
                
                lock_hex_text = QTextEdit(readOnly=True, plainText=lock_tx.serialize().hex())
                unlock_hex_text = QTextEdit(readOnly=True, plainText=unlock_tx.serialize().hex())

                tx_layout.addWidget(lock_hex_text)
                tx_layout.addWidget(unlock_hex_text)

                tx_tab_widget.addTab(tx_widget, "Transactions")

                tab_widget.addTab(tx_tab_widget, "Transactions")

        prepare_button.clicked.connect(on_prepare_button_clicked)

        tab_widget.update()

    def restrict_output(self, wallet, address, amount, tab_widget):

        self.out_public_key = bytes.fromhex(wallet.get_public_keys(self.address_text_fields[wallet].text())[0])
        self.out_amount = float(self.amount_text_field.text())

        input_selection_widget = QWidget()
        layout = QVBoxLayout(input_selection_widget)

        tab_widget.addTab(input_selection_widget, "Input selection")

        self.signal_emitter.show_inputlist_signal.emit(tab_widget, input_selection_widget, wallet)

    def sha256(self, input):
        return hashlib.sha256(input).digest()

    def get_txid(self, tx):
        return tx.GetTxid()[::-1]

    def create_template_hash(self, tx: CTransaction, nIn: int) -> bytes:
        r = b""
        r += struct.pack("<i", tx.nVersion)
        r += struct.pack("<I", tx.nLockTime)
        vin = tx.vin or []
        vout = tx.vout or []

        if any(inp.scriptSig for inp in vin):
            r += self.sha256(b"".join(ser_string(inp.scriptSig) for inp in vin))
        r += struct.pack("<I", len(tx.vin))
        r += self.sha256(b"".join(struct.pack("<I", inp.nSequence) for inp in vin))
    
        r += struct.pack("<I", len(tx.vout))

        r += self.sha256(b"".join(out.serialize() for out in vout))
        r += struct.pack("<I", nIn)
        return hashlib.sha256(r).digest()


    def ctv_template(self, AMOUNT: int = None):
        tx = CMutableTransaction()
        tx.nVersion = 2

        tx.vin = [CMutableTxIn()]

        lock_script = CScript([script.OP_0, script.Hash160(self.out_public_key)])
        tx.vout.append(CTxOut(AMOUNT, lock_script))

        return tx

    def spend_tx(self, amount: int=None, vin_txid=None, vin_index=None):
        tx = self.ctv_template(amount)
        tx.vin = [CTxIn(COutPoint(lx(vin_txid), vin_index), nSequence=0xffffffff)]
        return tx

    def ctv_tx(self, amount: int = None, vin_txid: str = None, vin_index: int = None, utxo_address=None, funding_prvkey=None, funding_pubkey=None):
        template = self.ctv_template(AMOUNT=amount - self.TX_FEE)
        ctv_hash = self.create_template_hash(template, 0)
        tx = CMutableTransaction()

        tx.vin = [CTxIn(COutPoint(lx(vin_txid), vin_index))]
        tx.vout = [CTxOut(amount - self.TX_FEE, CScript([ctv_hash, self.OP_CHECKTEMPLATEVERIFY]))]

        redeem_script = utxo_address.to_redeemScript()
        sighash = script.SignatureHash(
            script=redeem_script,
            txTo=tx,
            inIdx=0,
            hashtype=script.SIGHASH_ALL,
            amount=amount,
            sigversion=script.SIGVERSION_WITNESS_V0,
        )
        signature = funding_prvkey.sign(sighash) + bytes([script.SIGHASH_ALL])

        tx.wit = CTxWitness([CTxInWitness(CScriptWitness([signature, funding_pubkey]))])

        return tx